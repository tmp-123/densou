<?php declare(strict_types=1);

namespace Densou\Domain\Exception;

use Densou\Domain\Exception;

class InvalidArgumentException extends Exception
{
}

