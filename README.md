### Notice
I haven't finished project, but I hope it is enough code to get feeling of my coding practices.

### TODO:
- [ ] resolve issue with nginx or phalcon configuration so other controllers are accessiable (right now every call goes to IndexController)
- [ ] add sorting on fronted (on backend it's done)
- [ ] handle gracefully 403 response status from gtihub (github has strong limitations on number of api calls per hour)
- [ ] add GithubGatewayStub to allow isolated behat tests
- [ ] cache cleaning from old entries OR moving to redis or memcached


### Project installation

1. Install docker and docker-compose

[docker installation guide](https://docs.docker.com/engine/installation/)

[docker-compose installationg guide](https://docs.docker.com/compose/install/)


2. Build docker

```
docker-compose build
```

3. Run docker-compose

```
docker-compose up -d
```

4. Enter docker php container

```
docker exec -it densoutesttask_php_1 bash
```

5. Download vendors

```
composer install
```

6. Load database schema

```
psql -h internal-db -U densou -d densou -1 -f schema/densou.sql
```

7. On your host system go to

```
http://localhost:8100/
```


### Tests 

I wrote few tests, didn't have enough time so skipped testing :(. 
Test can be run by

```
php bin/behat
php bin/phpunit
```


### Writing: 
<i>
Write 10 lines about how this application could support fetching data from more than GitHub. Let us say we wanted to fetch data from Stack Overflow and LinkedIn too. How should this be done with tables in a database, organising code in classes in the project, etc.
</i>

I see two solutions:
* we can treat other search engines as new contexts and add completly new logic for them. Using Github search as an example.
* we can make githubGateway interface more general. Rename it to ex. SearchEngineGateway. Add searchType attribute which would determine to what search call for results should be make. In SearchEngineGateway implementation we could use strategy pattern to choose api implementation depending on searchType variable. SearchType would be added to SearchModel to include search type in history. This way we could easily display searching from all search engines on one list.





# Small test task for Densou Trading Desk

### Introduction

We have a couple of things that we would want to feel you out on.

 1. How well do you handle popular frameworks? Do you use built-in functionality or do you write functionality yourself?
 2. Architecture. Do you have a gut feeling for scaling applications.
 3. Current standards and best practice. Do you use them, and is your code easy for other developers to debug and maintain.

### What do we have?

This is an almost empty PhalconPHP skeleton project. Information about the framework can be found here https://phalconphp.com/

There is a page with a form, in where you can fill in text, and a submit button.

### What do we want?

1. Coding: When you type in a github repo, for example "laravel/laravel" we want all the contributors to that repo, listed below with their name, picture (avatar) and number of contributions.  
    1.  It should be possible to sort the contributors, by contributions or name.
    2.  Also we want recent searches saved in a database. This list should be shown on a different page.
2. Writing: Write 10 lines about how this application could support fetching data from more than GitHub. Let us say we wanted to fetch data from Stack Overflow and LinkedIn too. How should this be done with tables in a database, organising code in classes in the project, etc.

