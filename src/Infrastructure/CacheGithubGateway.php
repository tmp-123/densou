<?php declare(strict_types=1);

namespace Densou\Infrastructure;

use Densou\Domain\GithubRepo;
use Densou\Domain\Models\Cache;
use Densou\Query\GithubGateway;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\OrderDirection;

class CacheGithubGateway implements GithubGateway
{
    /**
     * @var HttpGithubGateway
     */
    private $httpGithubGateway;

    /**
     * @var ContributorsSorter
     */
    private $sorter;

    public function __construct(HttpGithubGateway $httpGithubGateway, ContributorsSorter $sorter)
    {
        $this->httpGithubGateway = $httpGithubGateway;
        $this->sorter = $sorter;
    }

    public function findContributors(GithubRepo $repo, OrderColumn $column, OrderDirection $direction) : array
    {
        $result = $this->loadFromCache($repo);
        if ($result) {
            $results = $this->sorter->sort($results, $column, $direction);

            return $result;
        }

        $results = $this->httpGithubGateway->findContributors($repo, $column, $direction);
        $this->addToCache($repo, $results);

        return $results;
    }

    private function loadFromCache(GithubRepo $repo)
    {
        $repo = (string) $repo;
        $cache = Cache::findFirst("key ='{$repo}'");
        if ($cache) {
            return unserialize(base64_decode($cache->content));
        }

        return null;
    }

    private function addToCache(GithubRepo $repo, array $result)
    {
        $cache = new Cache();
        $cache->key = (string) $repo;
        $cache->content = base64_encode(serialize($result));
        $cache->createdat = time();
        $cache->save();
    }
}
