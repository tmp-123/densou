<?php declare(strict_types=1);

namespace Densou\UseCase;

use Densou\Domain\Models\Search;
use Densou\Command;
use Densou\UseCase;

final class AddSearchUseCase implements UseCase
{
    private $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param AddSearchCommand $command
     */
    public function handle(Command $command)
    {
        $search = new Search();
        $search->searchedfor = $command->getSearchedFor();
        $search->createdat = time();

        if (!$search->save()) {
            $this->logger->error(implode(',', $search->getMessages()));
        }
    }
}
