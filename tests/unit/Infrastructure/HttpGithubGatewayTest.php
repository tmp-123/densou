<?php

namespace Test\Infrastructure;

use Densou\Domain\GithubRepo;
use Densou\Infrastructure\ContributorsSorter;
use Densou\Infrastructure\HttpGithubGateway;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\OrderDirection;

class HttpGithubGatewayTest extends \UnitTestCase
{
    /**
     * @expectedException Densou\Query\GithubGateway\RepoNotFoundException
     */
    public function test_it_throws_exception_when_repo_not_found()
    {
        $repoStub = $this->createMock(\GitHubRepos::class);
        $repoStub->method('listContributors')
             ->will($this->throwException(new \GitHubClientException('Expected status [200], actual status [404], URL [dummy/test]')));

        $githubGateway = new \GitHubClient();
        $githubGateway->repos = $repoStub;
        $githubGateway = new HttpGithubGateway($githubGateway, new ContributorsSorter());

        $githubGateway->findContributors(
            new GithubRepo('what/ever'),
            OrderColumn::byContributions(),
            OrderDirection::desc()
        );
    }
}
