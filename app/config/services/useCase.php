<?php

$di->set(
    "use_case.add_search",
    [
        "className" => "Densou\\UseCase\\AddSearchUseCase",
        "arguments" => [
            [
                "type" => "service",
                "name" => "logger",
            ],
        ]
    ]
);