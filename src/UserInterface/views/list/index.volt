{{ content() }}

<ul class="pager">
    <li class="previous pull-left">
        {{ link_to("index/index", "&larr; Go Back") }}
    </li>
</ul>

{% for search in page.items %}
{% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Search phrase</th>
            <th>Date</th>
        </tr>
    </thead>
{% endif %}
    <tbody>
        <tr>
            <td>{{ search.searchedfor }}</td>
            <td>{{ date("Y-m-d H:i:s", search.createdat }}</td>
        </tr>
    </tbody>
{% if loop.last %}
    <tbody>
        <tr>
            <td colspan="10" align="right">
                <div class="btn-group">
                    {{ link_to("list/index", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("list/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn ") }}
                    {{ link_to("list/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("list/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
{% endif %}
{% else %}
    No entries found
{% endfor %}
