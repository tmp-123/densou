<?php declare(strict_types=1);

namespace Densou\Infrastructure;

use Densou\Query\GithubGateway\Contributor;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\OrderDirection;

class ContributorsSorter
{
    /**
     * @param Contributor[] $contributors
     * @param OrderColumn $column
     * @param OrderDirection $direction
     * @return Contributor[]
     */
    public function sort(array $contributors, OrderColumn $column, OrderDirection $direction) : array
    {
        $directionAsInt = $direction->toInt();

        switch ((string) $column) {
            case OrderColumn::ORDER_BY_NAME:
                usort(
                    $contributors,
                    function (Contributor $a, Contributor $b) use ($directionAsInt) {
                        return ($a->name <=> $b->name) * $directionAsInt;
                    }
                );
                break;
            case OrderColumn::ORDER_BY_CONTRIBUTIONS:
                usort(
                    $contributors,
                    function (Contributor $a, Contributor $b) use ($directionAsInt) {
                        return ($a->contributions <=> $b->contributions) * $directionAsInt;
                    }
                );
                break;
        }

        return $contributors;
    }
}
