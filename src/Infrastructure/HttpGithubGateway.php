<?php declare(strict_types=1);

namespace Densou\Infrastructure;

use Densou\Domain\GithubRepo;
use Densou\Query\GithubGateway;
use Densou\Query\GithubGateway\Contributor;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\GithubGateway\RepoNotFoundException;
use Densou\Query\OrderDirection;

class HttpGithubGateway implements GithubGateway
{
    const GITHUB_CLIENT_EXCEPTION_404_MESSAGE_BEGINNING = 'Expected status [200], actual status [404]';
    const MAX_ALLOWED_PAGESIZE = 30;

    /**
     * @var \GitHubClient
     */
    private $githubClient;

    /**
     * @var ContributorsSorter
     */
    private $sorter;

    /**
     * @var int
     */
    private $page;

    public function __construct(\GitHubClient $gitHubClient, ContributorsSorter $sorter)
    {
        $this->githubClient = $gitHubClient;
        $this->sorter = $sorter;

        $this->page = 1;

        $this->setPageData();
    }

    public function findContributors(GithubRepo $repo, OrderColumn $column, OrderDirection $direction) : array
    {
        try {
            $contributors = $this->githubClient->repos->listContributors(
                $repo->getOwner(),
                $repo->getRepo()
            );

            while (true) {
                $newContributors = $this->getNextPage($repo);
                if (!$newContributors) {
                    break;
                }

                $contributors = array_merge($contributors, $newContributors);
            }

            $results = $this->createContributorResults($contributors);
            $results = $this->sorter->sort($results, $column, $direction);

        } catch (\GitHubClientException $e) {
            $msg = $e->getMessage();
            if (0 === strpos($msg, self::GITHUB_CLIENT_EXCEPTION_404_MESSAGE_BEGINNING)) {
                throw new RepoNotFoundException();
            }
            throw $e; // rethrow all other exceptions
        }

        return $results;
    }

    private function createContributorResults(array $result)
    {
        return array_map(
            function (\GitHubContributor $contributor) {
                return new Contributor(
                    $contributor->getLogin(),
                    $contributor->getAvatarUrl(),
                    (int) $contributor->getContributions()
                );
            },
            $result
        );
    }

    private function getNextPage(GithubRepo $repo)
    {
        $this->page++;
        $this->githubClient = new \GitHubClient(); // reset object duo to problems with page incrementing

        $this->setPageData();

        $contributors = $this->githubClient->repos->listContributors(
            $repo->getOwner(),
            $repo->getRepo()
        );
        if (!$contributors) {
            return null;
        }

        return $contributors;
    }

    private function setPageData() : void
    {
        $this->githubClient->setPage($this->page);
        $this->githubClient->setPageSize(self::MAX_ALLOWED_PAGESIZE);
    }
}
