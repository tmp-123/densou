<?php

$di->set(
    "query.github.client",
    [
        "className" => "\\GitHubClient",
    ]
);

$di->set(
    "query.github.contributors_sorter",
    [
        "className" => "Densou\\Infrastructure\\ContributorsSorter",
    ]
);

$di->set(
    "query.github.http",
    [
        "className" => "Densou\\Infrastructure\\HttpGithubGateway",
        "arguments" => [
            [
                "type" => "service",
                "name" => "query.github.client",
            ],
            [
                "type" => "service",
                "name" => "query.github.contributors_sorter",
            ]
        ]
    ]
);

$di->set(
    "query.github",
    [
        "className" => "Densou\\Infrastructure\\CacheGithubGateway",
        "arguments" => [
            [
                "type" => "service",
                "name" => "query.github.http",
            ],
            [
                "type" => "service",
                "name" => "query.github.contributors_sorter",
            ]
        ]
    ]
);
