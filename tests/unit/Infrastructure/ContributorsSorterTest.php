<?php

namespace Test\Infrastructure;

use Densou\Infrastructure\ContributorsSorter;
use Densou\Query\GithubGateway\Contributor;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\OrderDirection;

class ContributorsSorterTest extends \UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->results = [
            new Contributor('John', null, 7),
            new Contributor('Alex', null, 10),
            new Contributor('Kate', null, 9),
        ];
    }

    public function test_it_sorts_by_contributions_descending()
    {
        $sorter = new ContributorsSorter();
        $actual = $sorter->sort($this->results, OrderColumn::byContributions(), OrderDirection::desc());
        $expected = [
            new Contributor('Alex', null, 10),
            new Contributor('Kate', null, 9),
            new Contributor('John', null, 7),
        ];

        $this->assertEquals($actual, $expected);
    }

    public function test_it_sorts_by_contributions_ascending()
    {
        $sorter = new ContributorsSorter();
        $actual = $sorter->sort($this->results, OrderColumn::byContributions(), OrderDirection::asc());
        $expected = [
            new Contributor('John', null, 7),
            new Contributor('Kate', null, 9),
            new Contributor('Alex', null, 10),
        ];

        $this->assertEquals($actual, $expected);
    }

    public function test_it_sorts_by_name_descending()
    {
        $sorter = new ContributorsSorter();
        $actual = $sorter->sort($this->results, OrderColumn::byName(), OrderDirection::desc());
        $expected = [
            new Contributor('Kate', null, 9),
            new Contributor('John', null, 7),
            new Contributor('Alex', null, 10),
        ];

        $this->assertEquals($actual, $expected);
    }

    public function test_it_sorts_by_name_ascending()
    {
        $sorter = new ContributorsSorter();
        $actual = $sorter->sort($this->results, OrderColumn::byName(), OrderDirection::asc());
        $expected = [
            new Contributor('Alex', null, 10),
            new Contributor('John', null, 7),
            new Contributor('Kate', null, 9),
        ];

        $this->assertEquals($actual, $expected);
    }
}
