Feature: Search validation
  In order to search github repositories
  As a website user
  I want to be notify if I enter incorrect phrase in search input

  Scenario Outline: Validate search
    Given I am on "/"
    When I fill in "repository-search-input" with "<input>"
    And I press "repository-search-submit"
    Then I should see "<message>"
  Examples:
    | input          | message                           |
    |                | The repository is required        |
    | d/             | Repository string is too short    |
    | Seldaekmonolog | Repository need to have two parts |




