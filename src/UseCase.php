<?php declare(strict_types=1);

namespace Densou;

interface UseCase
{
    public function handle(Command $command);
}
