<?php declare(strict_types=1);

namespace Densou\Query;

use Densou\Domain\GithubRepo;
use Densou\Query\GithubGateway\Contributor;
use Densou\Query\GithubGateway\OrderColumn;

interface GithubGateway
{
    /**
     * @param GithubRepo $repo
     * @param OrderColumn $column
     * @param OrderDirection $direction
     * @return Contributor[]
     */
    public function findContributors(GithubRepo $repo, OrderColumn $column, OrderDirection $direction) : array;
}
