<?php declare(strict_types=1);

namespace Densou\Domain\Models;

use Phalcon\Mvc\Model;

class Cache extends Model
{
    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $content;

    /**
     * @var integer
     */
    public $createdat;
}

