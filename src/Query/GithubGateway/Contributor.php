<?php declare(strict_types=1);

namespace Densou\Query\GithubGateway;

final class Contributor
{
    public $name;
    public $avatarUrl;
    public $contributions;

    public function __construct(
        string $name,
        string $avatarUrl = null,
        int $contributions
    ) {
        $this->name = $name;
        $this->avatarUrl = $avatarUrl;
        $this->contributions = $contributions;
    }
}
