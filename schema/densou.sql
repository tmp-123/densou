
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cache; Type: TABLE; Schema: public; Owner: densou
--

CREATE TABLE cache (
    key character varying(32) NOT NULL,
    content text NOT NULL,
    createdat integer NOT NULL
);


ALTER TABLE cache OWNER TO densou;

--
-- Name: search; Type: TABLE; Schema: public; Owner: densou
--

CREATE TABLE search (
    id integer NOT NULL,
    searchedfor character varying(255) NOT NULL,
    createdat integer NOT NULL
);


ALTER TABLE search OWNER TO densou;

--
-- Name: search_id_seq; Type: SEQUENCE; Schema: public; Owner: densou
--

CREATE SEQUENCE search_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE search_id_seq OWNER TO densou;

--
-- Name: search_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: densou
--

ALTER SEQUENCE search_id_seq OWNED BY search.id;


--
-- Name: search id; Type: DEFAULT; Schema: public; Owner: densou
--

ALTER TABLE ONLY search ALTER COLUMN id SET DEFAULT nextval('search_id_seq'::regclass);


--
-- Name: cache cache_pkey; Type: CONSTRAINT; Schema: public; Owner: densou
--

ALTER TABLE ONLY cache
    ADD CONSTRAINT cache_pkey PRIMARY KEY (key);


--
-- Name: search search_pkey; Type: CONSTRAINT; Schema: public; Owner: densou
--

ALTER TABLE ONLY search
    ADD CONSTRAINT search_pkey PRIMARY KEY (id);

--
-- PostgreSQL database dump complete
--
