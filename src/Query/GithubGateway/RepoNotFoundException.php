<?php declare(strict_types=1);

namespace Densou\Query\GithubGateway;

use Densou\Domain\Exception;

class RepoNotFoundException extends Exception
{
}
