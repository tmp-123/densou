<?php declare(strict_types=1);

namespace Densou\Query\GithubGateway;

use Densou\Domain\Exception\InvalidArgumentException;

final class OrderColumn
{
    const ORDER_BY_CONTRIBUTIONS = 'contributions';
    const ORDER_BY_NAME = 'name';

    /**
     * @var string
     */
    private $orderBy;

    public function __construct(string $orderBy)
    {
        $orderBy = mb_strtolower($orderBy);
        if (!$this->isValidOrderBy($orderBy)) {
            throw new InvalidArgumentException(sprintf('Invalid orderBy %s', $orderBy));
        }

        $this->orderBy = $orderBy;
    }

    public static function byContributions() : self
    {
        return new self(self::ORDER_BY_CONTRIBUTIONS);
    }

    public static function byName() : self
    {
        return new self(self::ORDER_BY_NAME);
    }

    public function __toString()
    {
        return $this->orderBy;
    }

    private function isValidOrderBy(string $orderBy)
    {
        return in_array(
            $orderBy,
            [self::ORDER_BY_CONTRIBUTIONS, self::ORDER_BY_NAME]
        );
    }
}
