<?php

namespace Test\Query;

use Densou\Query\OrderDirection;

class OrderDirectionTest extends \UnitTestCase
{
    /**
     * @dataProvider data_provider_valid_values_for_order_direction
     */
    public function test_it_creates_order_direction($validValue)
    {
        $this->assertInstanceOf(OrderDirection::class, new OrderDirection($validValue));

    }

    public function data_provider_valid_values_for_order_direction()
    {
        return [
            ['asc'],
            ['ASC'],
            ['desc'],
            ['DESC'],
            ['aSC'],
            ['Asc'],
            ['dEsC'],
        ];
    }

    /**
     * @expectedException Densou\Domain\Exception\InvalidArgumentException
     * @dataProvider data_provider_invalid_values_for_order_direction
     */
    public function test_it_throws_exception_when_created_with_invalid_value($invalidValue)
    {
        new OrderDirection($invalidValue);
    }

    public function data_provider_invalid_values_for_order_direction()
    {
        return [
            [''],
            ['asc1'],
            ['ascx'],
            ['up'],
        ];
    }

    public function test_casting_to_string()
    {
        $this->assertEquals(OrderDirection::ASC, (string) new OrderDirection('asc'));
        $this->assertEquals(OrderDirection::ASC, (string) new OrderDirection('ASC'));
        $this->assertEquals(OrderDirection::DESC, (string) new OrderDirection('desc'));
        $this->assertEquals(OrderDirection::DESC, (string) new OrderDirection('DESC'));
    }

    public function test_creating_order_direction_by_name_contructors()
    {
        $asc = OrderDirection::asc();
        $desc = OrderDirection::desc();

        $this->assertInstanceOf(OrderDirection::class, $asc);
        $this->assertInstanceOf(OrderDirection::class, $desc);
        $this->assertEquals(OrderDirection::ASC, (string) $asc);
        $this->assertEquals(OrderDirection::DESC, (string) $desc);
    }
}