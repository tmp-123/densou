Feature: Index page
  In order to use website
  As a website user
  I need to be able to load home page

  Scenario: Load index page
    Given I am on "/"
    Then I should see "Github contributor lookup"
