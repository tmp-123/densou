<?php declare(strict_types=1);

namespace Densou\Query;

use Densou\Domain\Exception\InvalidArgumentException;

final class OrderDirection
{
    const ASC = 'ASC';
    const DESC = 'DESC';

    /**
     * @var string
     */
    private $direction;

    public function __construct(string $direction)
    {
        $direction = mb_strtoupper($direction);
        if (!$this->isValidValue($direction)) {
            throw new InvalidArgumentException(
                sprintf('Direction %s does not match allowed order direction types.', $direction)
            );
        }

        $this->direction = $direction;
    }

    public static function asc() : self
    {
        return new self(self::ASC);
    }

    public static function desc() : self
    {
        return new self(self::DESC);
    }

    public function __toString() : string
    {
        return $this->direction;
    }

    public function toInt() : int
    {
        if ($this->direction === self::ASC) {
            return 1;
        }

        return -1;
    }

    private function isValidValue(string $value) : bool
    {
        return in_array($value, [self::ASC, self::DESC]);
    }
}
