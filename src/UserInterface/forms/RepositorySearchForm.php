<?php declare(strict_types=1);

namespace Densou\UserInterface\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class RepositorySearchForm extends Form
{
    public function initialize() : void
    {
        $repository = new Text('repository', [
            'placeholder' => 'github repo',
            'id' => 'repository-search-input',
        ]);

        $repository->addValidators([
            new PresenceOf([
                'message' => 'The repository is required'
            ]),
            new StringLength([
                'min' => 3,
                'messageMinimum' => 'Repository string is too short. Minimum 3 characters'
            ]),
            new StringLength([
                'max' => 255,
                'messageMinimum' => 'Repository string is too long. Maxium 255 characters'
            ]),
            new Callback([
                'callback' => function($data) {
                    $repositoryPartsCount = count(explode('/', $data['repository']));

                    return $repositoryPartsCount === 2;
                },
                'message' => 'Repository need to have two parts. Divided by slash symbol ex. Seldaek/monolog'
            ]),
        ]);

        $this->add($repository);
        $this->add(new Submit('search', [
            'id' => 'repository-search-submit',
        ]));
    }
}
