<?php declare(strict_types=1);

namespace Densou\UserInterface\Controllers;

use Densou\Domain\Models\Search;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ListController extends ControllerBase
{
    public function indexAction() : void
    {
        $numberPage = $this->request->getQuery('page', 'int') ?: 1;

        $searches = Search::find();

        $paginator = new Paginator([
            'data' => $searches,
            'limit' => 5,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }
}

