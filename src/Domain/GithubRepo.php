<?php declare(strict_types=1);

namespace Densou\Domain;

final class GithubRepo
{
    private $owner;
    private $repo;

    public function __construct(string $repository)
    {
        $repository = trim($repository);
        $repositoryParts = explode('/', $repository);

        if (count($repositoryParts) !== 2) {
            throw new Exception('Repository must have 2 parts split by slash');
        }

        $this->owner = $repositoryParts[0];
        $this->repo = $repositoryParts[1];
    }

    public function __toString() : string
    {
        return $this->owner.'/'.$this->repo;
    }

    public function getOwner() : string
    {
        return $this->owner;
    }

    public function getRepo() : string
    {
        return $this->repo;
    }
}

