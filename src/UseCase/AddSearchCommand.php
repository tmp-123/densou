<?php declare(strict_types=1);

namespace Densou\UseCase;

use Densou\Command;

final class AddSearchCommand implements Command
{
    private $searchedFor;

    public function __construct(string $searchedFor)
    {
        $this->searchedFor = $searchedFor;
    }

    public function getSearchedFor() : string
    {
        return $this->searchedFor;
    }
}
