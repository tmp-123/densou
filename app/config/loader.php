<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'Densou\Models' => $config->application->modelsDir,
    'Densou\UserInterface\Controllers' => $config->application->controllersDir,
    'Densou\UserInterface\Forms' => $config->application->formsDir,
//    'Densou'             => $config->application->libraryDir
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';
