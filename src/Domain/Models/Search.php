<?php declare(strict_types=1);

namespace Densou\Domain\Models;

use Phalcon\Mvc\Model;

class Search extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $searchedfor;

    /**
     * @var integer
     */
    public $createdat;

}

