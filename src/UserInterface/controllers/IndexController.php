<?php declare(strict_types=1);

namespace Densou\UserInterface\Controllers;

use Densou\Domain\GithubRepo;
use Densou\Domain\Models\Cache;
use Densou\Domain\Models\Search;
use Densou\Query\GithubGateway;
use Densou\Query\GithubGateway\OrderColumn;
use Densou\Query\GithubGateway\RepoNotFoundException;
use Densou\Query\OrderDirection;
use Densou\UseCase\AddSearchCommand;
use Phalcon\Tag;
use Densou\UserInterface\Forms\RepositorySearchForm;
use Phalcon\Paginator\Adapter\Model as Paginator;

class IndexController extends ControllerBase
{
    const LIMIT = 10;

    public function indexAction() : void
    {
        $this->listHistory();

        $form = new RepositorySearchForm();
        $this->view->form = $form;

        if (!$this->request->isPost()) {
            return;
        }

        $isValid = $form->isValid($this->request->getPost());
        if (!$isValid) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return;
        }

        $repo = $this->request->getPost('repository', 'striptags');

        try {
            /** @var GithubGateway $queryGithub */
            $queryGithub = $this->getDi()->get('query.github');
            $contributors = $queryGithub->findContributors(
                new GithubRepo($repo),
                OrderColumn::byContributions(),
                OrderDirection::desc()
            );
            $this->view->contributors = $contributors;

        } catch (RepoNotFoundException $e) {
            $this->flash->error(sprintf('Repo %s not found', $repo));
        }

        $addSearchUseCase = $this->getDi()->get('use_case.add_search');
        $addSearchUseCase->handle(new AddSearchCommand($repo));
    }

    private function listHistory()
    {
        $numberPage = $this->request->getQuery('page', 'int') ?: 1;

        $searches = Search::find();

        $paginator = new Paginator([
            'data' => $searches,
            'limit' => self::LIMIT,
            'page' => $numberPage,
        ]);

        $this->view->page = $paginator->getPaginate();
    }
}

