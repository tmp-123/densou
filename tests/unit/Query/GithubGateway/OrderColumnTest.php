<?php

namespace Test\Query\GithubGateway;

use Densou\Query\GithubGateway\OrderColumn;

class OrderColumnTest extends \UnitTestCase
{
    /**
     * @dataProvider data_provider_valid_values_for_order_column
     */
    public function test_it_creates_order_column($validValue)
    {
        $this->assertInstanceOf(OrderColumn::class, new OrderColumn($validValue));

    }

    public function data_provider_valid_values_for_order_column()
    {
        return [
            ['contributions'],
            ['CONTRIBUTIONS'],
            ['name'],
            ['NAME'],
            ['Name'],
            ['coNTRibutions'],
        ];
    }

    /**
     * @expectedException Densou\Domain\Exception\InvalidArgumentException
     * @dataProvider data_provider_invalid_values_for_order_column
     */
    public function test_it_throws_exception_when_created_with_invalid_value($invalidValue)
    {
        new OrderColumn($invalidValue);
    }

    public function data_provider_invalid_values_for_order_column()
    {
        return [
            ['avatar'],
            [''],
        ];
    }

    public function test_casting_to_string()
    {
        $this->assertEquals(OrderColumn::ORDER_BY_NAME, (string) new OrderColumn('name'));
        $this->assertEquals(OrderColumn::ORDER_BY_NAME, (string) new OrderColumn('NAME'));
        $this->assertEquals(OrderColumn::ORDER_BY_CONTRIBUTIONS, (string) new OrderColumn('contributions'));
        $this->assertEquals(OrderColumn::ORDER_BY_CONTRIBUTIONS, (string) new OrderColumn('conTRIBUTIons'));
    }

    public function test_creating_order_column_by_name_contructors()
    {
        $byContributions = OrderColumn::byContributions();
        $byName = OrderColumn::byName();

        $this->assertInstanceOf(OrderColumn::class, $byContributions);
        $this->assertInstanceOf(OrderColumn::class, $byName);
        $this->assertEquals(OrderColumn::ORDER_BY_CONTRIBUTIONS, (string) $byContributions);
        $this->assertEquals(OrderColumn::ORDER_BY_NAME, (string) $byName);
    }
}