<div class="page-header">
    <h1>Github contributor lookup</h1>
</div>

{{ content() }}

<div align="left" class="well">
    {{ form('class': 'form-search') }}
    {{ form.render('repository') }}
    {{ form.render('search') }}
    {{ end_form() }}
</div>


{% if contributors is defined %}
<ul class="contributors">
{% for contributor in contributors %}
    <li class="contributor">
        <a>
            <img src="{{ contributor.avatarUrl }}"" width="64" height="64">
            <span class="contributor-name">{{ contributor.name }}</span>
            <span class="contributor-contributions">{{ contributor.contributions }}</span>
        </a>
    </li>
{% else %}
    <li class="contributor">
        No entries found
    </li>
{% endfor %}
</ul>
{% endif %}


<h2>Search history</h2>
{% for search in page.items %}
{% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Search phrase</th>
            <th>Date</th>
        </tr>
    </thead>
{% endif %}
    <tbody>
        <tr>
            {% set github_link = "https://github.com/" ~ search.searchedfor %}
            <td><a href="{{ github_link }}">{{ search.searchedfor }}</a></td>
            <td>{{ date("Y-m-d H:i:s", search.createdat) }}</td>
        </tr>
    </tbody>
{% if loop.last %}
    <tbody>
        <tr>
            <td colspan="10" align="right">
                <div class="btn-group">
                    {{ link_to("/", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("/?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn ") }}
                    {{ link_to("/?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("/?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    <tbody>
</table>
{% endif %}
{% else %}
    No entries found
{% endfor %}
